# [SOGo](https://sogo.nu/) Packages

This project basically just builds SOGo packages according to the [instructions](http://wiki.sogo.nu/Compilation).

## Support

If you like what i'm doing, you can support me via [Paypal](https://paypal.me/morph027), [Ko-Fi](https://ko-fi.com/morph027) or [Patreon](https://www.patreon.com/morph027).

## Add repo signing key to apt

```bash
sudo curl -sL -o /etc/apt/trusted.gpg.d/morph027-sogo.asc https://packaging.gitlab.io/sogo/gpg.key
```

## Add repo to apt

```bash
echo "deb [arch=amd64] https://packaging.gitlab.io/sogo/buster buster main" | sudo tee /etc/apt/sources.list.d/morph027-sogo-buster.list
```

## Install packages

```bash
sudo apt-get update
sudo apt-get install sogo morph027-keyring
```

## Extras

### unattended-upgrades

To enable automatic upgrades using `unattended-upgrades`, just add the following config file:

```bash
cat > /etc/apt/apt.conf.d/50sogo <<EOF
Unattended-Upgrade::Origins-Pattern {
	"origin=morph027,codename=${distro_codename},label=sogo";
};
EOF
```

# DEV

## Build packages

```bash
CODENAME=bookworm
docker buildx build --load -f ".docker/${CODENAME}/Dockerfile" -t "sogo:${CODENAME}" .
docker run --rm -it -v $PWD:/build --tmpfs /tmp:exec -w /tmp/ -e DEBUG=true -e CI_PROJECT_DIR=/build -e CCACHE_DIR=/ccache -v "sogo-${CODENAME}-ccache":/ccache -e CI_COMMIT_TAG=SOGo-5.9.1+1 -e CODENAME=${CODENAME} "sogo:${CODENAME}"
/build/.gitlab-ci/build.sh
```

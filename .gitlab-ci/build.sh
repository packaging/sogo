#!/bin/bash

set -e

if [[ -n "${DEBUG}" ]]; then
    set -x
fi

export PATH="/usr/lib/ccache/bin/:${PATH}"
export CCACHE_DIR="${CCACHE_DIR:-${CI_PROJECT_DIR}/ccache}"
mkdir -p "${CCACHE_DIR}"
tmpdir="$(mktemp -d)"

TAG="${CI_COMMIT_TAG#*-}"
VERSION="${TAG%%+*}"
PATCHLEVEL="${TAG##*+}"

if [[ ! "${PATCHLEVEL}" =~ ^[0-9]+$ ]]; then
  SOPE_BRANCH="${PATCHLEVEL}"
  SOGO_BRANCH="${PATCHLEVEL}"
else
  SOPE_BRANCH="SOPE-${VERSION}"
  SOGO_BRANCH="SOGo-${VERSION}"
fi

cd "${tmpdir}"
# build instructions: http://wiki.sogo.nu/Packaging
[ ! -d sope ] && git clone -b "${SOPE_BRANCH}" https://github.com/Alinto/sope
[ ! -d sogo ] && git clone -b "${SOGO_BRANCH}" https://github.com/Alinto/sogo
cd sope
cp -a packaging/debian debian
./debian/rules 
dpkg-buildpackage
cd "${tmpdir}"
dpkg -i ./*deb
cd sogo
cp -a packaging/debian debian
sed -i 's,^sogo (1.3.5),sogo ('${VERSION}+${PATCHLEVEL}'),' debian/changelog
./debian/rules
debuild \
    --preserve-envvar=CCACHE_DIR \
    --prepend-path=/usr/lib/ccache \
    --no-lintian \
    -b -us -uc \
    -j"$(getconf _NPROCESSORS_ONLN)"
cd "${tmpdir}"
mkdir -p "${CI_PROJECT_DIR}/release/${CODENAME}"
mv ./*.deb "${CI_PROJECT_DIR}/release/${CODENAME}"/
